const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
    path: 'data/error-14.csv',
    header: [
        { id: 'M204RECORD', title: 'M204RECORD' },
        { id: 'M204Group', title: 'M204Group' },
        { id: 'M204FIELD', title: 'M204FIELD' },
        { id: 'M204Type', title: 'M204Type' },
        { id: 'M204Length', title: 'M204Length' },
        { id: 'JSONGroup1', title: 'JSONGroup1' },
        { id: 'JSONGroup2', title: 'JSONGroup2' },
        { id: 'JSONFieldName', title: 'JSONFieldName' },
        { id: 'GroupNotFound', title: 'GroupNotFound' },
        { id: 'FieldNotFound', title: 'FieldNotFound' },
        { id: 'InvalidDataType', title: 'InvalidDataType' },
        { id: 'InvalidDataLength', title: 'InvalidDataLength' },
        { id: 'M204GroupEmpty', title: 'M204GroupEmpty' },
        { id: 'M204FieldEmpty', title: 'M204FieldEmpty' },
        { id: 'RecordNotFound', title: 'RecordNotFound' },
        { id: 'GroupNotUniquelyMapped', title: 'GroupNotUniquelyMapped' },
    ]
});


const fs = require('fs');
const jsonSchema = require('../data/json-schema-14.json');

const results = [];

const groupMap = new Map();

function populateGroupMap(m204Group, jsonGroup1) {
    let m204Groups = groupMap.get(jsonGroup1);
    if(m204Groups) {
        m204Groups.add(m204Group);
    } else {
        m204Groups = new Set();
        m204Groups.add(m204Group);
    }

    groupMap.set(jsonGroup1, m204Groups);
}

fs.createReadStream('data/field-mapping-14.csv')
    .pipe(csv())
    .on('data', (data) => {
        const record = { ...data, RecordNotFound: false, GroupNotFound: false, FieldNotFound: false, InvalidDataLength: false, InvalidDataType: false, M204GroupEmpty: false, M204FieldEmpty: false, GroupNotUniquelyMapped: false };
        const jsonGroup1 = data['JSONGroup1']
        const jsonGroup2 = data['JSONGroup2']
        const jsonField = data['JSONFieldName'];
        let m204Type = data['M204Type'];
        const m204Group = data['M204Group'];
        const m204Field = data['M204FIELD'];
        const m204Record = data['M204RECORD'];

        if (m204Record) {
            populateGroupMap(m204Group, jsonGroup1);
            switch (m204Type) {
                case 'CHARACTERS':
                case 'CHARACTER':
                case 'CHAR':
                case 'ALPHANUMERIC':
                case 'ALPHANUM':
                case 'ALPHA':
                    m204Type = 'string';
                    break;
                case 'NUMERIC':
                    m204Type = 'number';
                    break;
            }

            record["M204FieldEmpty"] = !m204Field || m204Field.trim().length <= 0;
            record["M204GroupEmpty"] = !m204Group || m204Group.trim().length <= 0;

            if (m204Group && m204Field) {
                const jsonDefinitions = jsonSchema['definitions'];
                const jsonSchemaGroup = jsonDefinitions[jsonGroup1];
                if (jsonSchemaGroup) {
                    const jsonSchemaField = jsonSchemaGroup['properties'][jsonField];
                    if (jsonSchemaField) {
                        record["InvalidDataType"] = (jsonSchemaField["type"] !== m204Type
                            && jsonSchemaField["format"] != "date"
                            && m204Type !== 'number'
                            && jsonSchemaField['type'] !== 'string');
                    } else {
                        record["FieldNotFound"] = true;
                    }
                } else {
                    record["GroupNotFound"] = true;
                }
            }
        } else {
            record["RecordNotFound"] = true; 
        }

        results.push(record);
    })
    .on('end', async () => {
        for(const record of results) {
            const jsonGroup1 = record['JSONGroup1'];
            const m204Groups = groupMap.get(jsonGroup1);
            if(m204Groups && m204Groups.size > 1) {
                record['GroupNotUniquelyMapped'] = true;
            }
        }
        
        await csvWriter.writeRecords(results);
    });



